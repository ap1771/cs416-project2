// File:	worker_t.h

// List all group member's name:
// username of iLab:
// iLab Server:

#define _XOPEN_SOURCE

#ifndef WORKER_T_H
#define WORKER_T_H

#define _GNU_SOURCE

/* To use Linux pthread Library in Benchmark, you have to comment the USE_WORKERS macro */
#define USE_WORKERS 1

#define LEVELS 4

/* include lib header files that you need here: */
#include <unistd.h>
#include <sys/time.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>	
#include <ucontext.h>
#include <stdbool.h>

typedef uint worker_t;

enum Status{Ready, Running, Blocked, Finished}; //determine the status of the thread

enum State{Ready_Waiting, unused, Locked};

typedef enum {INACTIVE, TIMER, YIELD, EXIT, JOIN, FIRST, BLOCKED} schedulerAction;


enum threadPriority {HIGH, MEDIUM, MEDIUM_LOW, LOW} ;
//determine the status of the mutex

typedef struct TCB {
	/* add important states in a thread control block */
	// thread Id
	worker_t tid;
	ucontext_t * cont;
	long sched;
	long startTime;
	//unsigned int thread_priority;
	worker_t jid;
	void * val_ptr;
	int mutex_id;
	// thread priority
	enum threadPriority priority;
	clock_t time_created;
	clock_t time_finished;
	clock_t first_time_scheduled;
	int first_scheduled;
	

	/* 0:highest priority; 1:Middle priority; 2:Lowest priority */
	// And more ...

	// YOUR CODE HERE
} TCB; 




/* mutex struct definition */
typedef struct worker_mutex_t {
	/* add something here */
	enum State state;
	int mutex_id;
	worker_t owner_id;

	// YOUR CODE HERE
} worker_mutex_t;

/*worker_mutexattr_t*/
typedef struct worker_mutexattr_t{

}worker_mutexattr_t;

/* define your data structures here: */
// Feel free to add your own auxiliary data structures (linked list or queue etc...)

// YOUR CODE HERE

typedef struct node{
    TCB *value;
	
    struct node* next;
}node_t;

typedef struct extied_process{
	//worker_t tid;
    TCB *process;
	struct extied_process * next;
	void * val_ptr;
	long endTime;
}extied_process;


typedef struct queue_t{
    node_t* head;
    node_t* tail;
}queue;

typedef struct list_t{
	node_t* head;
}list;

typedef struct schQueue {
	queue* highest_priority_queue;
    queue* medium_priority_queue;
    queue* medium_low_priority_queue;
    queue* lowest_priority_queue;
} schedQ;

/* Function Declarations: */

int queue_init(queue*);
int enqueue(node_t * newNode, queue * qu);
node_t* dequeue(queue* qu);
bool isEmpty(queue*);

list* initialize_list();
int add_to_list(TCB*);

void rapport();

void completeUnlock(worker_mutex_t *mutex, node_t *tmp);

void addToExitList(extied_process *exiting_pr);


/* create a new thread */
int worker_create(worker_t * thread, pthread_attr_t * attr, void
    *(*function)(void*), void * arg);

/* give CPU pocession to other user level worker threads voluntarily */
int worker_yield();

/* terminate a thread */
int worker_exit(void *value_ptr);

/* wait for thread termination */
int worker_join(worker_t thread, void **value_ptr);

/* initial the mutex lock */
int worker_mutex_init(worker_mutex_t *mutex, void **arg);

/* aquire the mutex lock */
int worker_mutex_lock(worker_mutex_t *mutex);

/* release the mutex lock */
int worker_mutex_unlock(worker_mutex_t *mutex);

void createSched();

/* destroy the mutex */
int worker_mutex_destroy(worker_mutex_t *mutex);

int updateRR();

void switchTimer();

void printThreads();

void calcuations();

void requeue();


void demote();


void cleanse();


void mlfqExit();


void mlfqJoin();

void mlfqBlock();

int updateMLFQ();


int getQueueLength(queue* queue);

ucontext_t* createExitPoint();


#ifdef USE_WORKERS
#define pthread_t worker_t
#define pthread_mutex_t worker_mutex_t
#define pthread_create worker_create
#define pthread_exit worker_exit
#define pthread_join worker_join
#define pthread_mutex_init worker_mutex_init
#define pthread_mutex_lock worker_mutex_lock
#define pthread_mutex_unlock worker_mutex_unlock
#define pthread_mutex_destroy worker_mutex_destroy
#define pthread_mutexattr_t worker_mutexattr_t
#endif

#endif
