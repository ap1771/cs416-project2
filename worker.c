// File:	worker.c

// List all group member's name:
// username of iLab:
// iLab Server:

#include "worker.h"

#include <sys/time.h>
#include <ucontext.h>
#include <stdio.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

// INITAILIZE ALL YOUR VARIABLES HERE
// YOUR CODE HERE
static void schedule();

static void sched_rr();

static void sched_mlfq();


node_t* current; //This thread control block points to the thread actually using the cpu.

ucontext_t sched;

struct sigaction act;

//#define MEM 64000 //This is the scheduler context

#define STACK_SIZE SIGSTKSZ  // for the stack of the context

int first = 0; //when worker_create is called for the first time, initialize all three queues

list* contexes;

float turnaround_times = 0.0; //This variable keep tracks of the turnaround times of all worker_threads created 
float response_times = 0.0; //This variable keeps track of the response times of all worker-thread created

uint thread_cnt = 1; //This variable keeps track of the number of threads created

schedQ * schedQueue = NULL;
queue * wait_queue = NULL;
node_t * current = NULL;
queue * join_queue = NULL;
extied_process * extied_processes = NULL;


int exitCount = 0;

schedulerAction schedulerFlag = FIRST;

int firstRun = 1;
int mutex_cnt = 0;

int maxCycleCount = 5;

struct itimerval it_val;

int currCycleCount = 0;

int enqueue(node_t * newNode, queue * qu){

	if(qu == NULL){
		qu = (queue*) malloc(sizeof(queue));
	}


	if(newNode == NULL) 
	   return -1;     

  
                                  
    //check if the queue is empty
    if(isEmpty(qu)){
        qu->head = newNode;
        qu->tail = newNode;
		newNode->next = NULL;
        return 0;
    }
    
    //add the element to the end of the queue
    else{
        qu->tail->next = newNode;
        qu->tail = newNode;
		qu->tail->next = NULL;
        return 0;
    }
}

node_t* dequeue(queue* qu){
	
	//check if the queue is empty
    if(qu == NULL || isEmpty(qu)) return NULL;

    node_t* result;
    result = qu->head;
    qu->head = result->next;

	//free(temp);

    return result;
}

bool isEmpty(queue* qu){
	return qu->head == NULL;
}

/*int add_to_list(TCB* t){

	if(contexes == NULL) contexes = (list*)malloc(sizeof(list));
	node_t* newNode = (node_t*)malloc(sizeof(node_t));

	newNode->value = t;
	newNode->next = NULL;

	if(contexes->head == NULL) contexes->head = &newNode;
	else{
        newNode->next = contexes->head;
		contexes->head = newNode;
	}
  return 0;
} */




ucontext_t * createExitPoint(){

	void (*e_function) (void *) = (void *)&worker_exit;
	ucontext_t *exit_context = (ucontext_t *) malloc(sizeof(ucontext_t));
	getcontext(exit_context);
	exit_context -> uc_link=NULL;
 	exit_context -> uc_stack.ss_sp=malloc(SIGSTKSZ);
 	exit_context -> uc_stack.ss_size=SIGSTKSZ;
 	exit_context -> uc_stack.ss_flags=0;
 	makecontext(exit_context, (void*)e_function, 1, NULL);

}
/* create a new thread */
int worker_create(worker_t * thread, pthread_attr_t * attr, 
                      void *(*function)(void*), void * arg) {


	ucontext_t * returnContext = createExitPoint();
	//Created Scehduler process

	

	

	
	ucontext_t *new_cont, *prev_cont;
	new_cont = (ucontext_t *) malloc(sizeof(ucontext_t));
	getcontext(new_cont);
	TCB * new_thread_tcb = (TCB *)malloc(sizeof(TCB));
	node_t * new_process = (node_t *) malloc(sizeof(node_t));
	//printf("first run: ");

	

    if(firstRun == 1){
		
		
		firstRun = 0;

		createSched();

		prev_cont = (ucontext_t *) malloc(sizeof(ucontext_t));
		node_t * old_process = (node_t *) malloc(sizeof(node_t));
		TCB * prev_thread_tcb = (TCB *)malloc(sizeof(TCB));
		getcontext(prev_cont);
		
		//Creates new thread context and new thread node 
		new_cont -> uc_link = returnContext;
 		new_cont -> uc_stack.ss_sp= malloc(SIGSTKSZ);
 		new_cont -> uc_stack.ss_size= SIGSTKSZ;
 		new_cont -> uc_stack.ss_flags=0;
 		makecontext(new_cont, (void*)function, 1, arg);

		new_thread_tcb -> cont = new_cont;
		new_thread_tcb -> tid = 1;
		new_thread_tcb -> priority = HIGH;
		new_thread_tcb -> jid = -1;
		new_thread_tcb -> val_ptr = NULL;
		new_thread_tcb -> mutex_id = -1;
		new_thread_tcb->time_created = clock(); //store the time the worker thread was created
		new_thread_tcb->first_scheduled = 0;
		
		new_process -> value = new_thread_tcb;
		new_process -> next = NULL;
		
		enqueue(new_process, schedQueue -> highest_priority_queue);
		
		//Gets old thread context and creates old thread node 
		prev_thread_tcb -> cont = prev_cont;
		prev_thread_tcb -> tid = 0;
		prev_thread_tcb -> priority = HIGH;
		prev_thread_tcb -> jid = -1;
		prev_thread_tcb -> val_ptr = NULL;
		prev_thread_tcb -> mutex_id = -1;
		
	    old_process -> value = prev_thread_tcb;
		old_process -> next = NULL;
		
		current = old_process;
		
		enqueue(old_process, schedQueue -> highest_priority_queue);



	}else{

		//printf("new thread reached ");
		new_cont -> uc_link = returnContext;
 		new_cont -> uc_stack.ss_sp= malloc(SIGSTKSZ);
 		new_cont -> uc_stack.ss_size= SIGSTKSZ;
 		new_cont -> uc_stack.ss_flags=0;
 		makecontext(new_cont, (void*)function, 1, arg);

		new_thread_tcb -> cont = new_cont;
		new_thread_tcb -> tid = thread_cnt;
		new_thread_tcb -> priority = HIGH;
		new_thread_tcb -> jid = -1;
		new_thread_tcb -> val_ptr = NULL;
		new_thread_tcb -> mutex_id = -1;
		new_thread_tcb->time_created = clock(); //store the time the worker thread was created;
		new_thread_tcb->first_scheduled = 0;
		
		new_process -> value = new_thread_tcb;
		new_process -> next = NULL;

		enqueue(new_process, schedQueue -> highest_priority_queue);
	}

	


	*thread = thread_cnt;
	thread_cnt++;

	schedule();

	return 0;
};

void createSched() {

	schedQueue = (schedQ *) malloc(sizeof(schedQ));


	schedQueue->highest_priority_queue = (queue *) malloc(sizeof(queue));
	schedQueue->highest_priority_queue->head = NULL;
	schedQueue->highest_priority_queue->tail = NULL;

	schedQueue->medium_priority_queue = (queue *) malloc(sizeof(queue));
	schedQueue->medium_priority_queue->head = NULL;
	schedQueue->medium_priority_queue->tail = NULL;


	schedQueue->medium_low_priority_queue = (queue *) malloc(sizeof(queue));
	schedQueue->medium_low_priority_queue->head = NULL;
	schedQueue->medium_low_priority_queue->tail = NULL;

	schedQueue->lowest_priority_queue = (queue *) malloc(sizeof(queue));
	schedQueue->lowest_priority_queue->head = NULL;
	schedQueue->lowest_priority_queue->tail = NULL;

	join_queue = (queue *) malloc(sizeof(queue));
	join_queue->head = NULL;
	join_queue->tail = NULL;

	wait_queue = (queue *) malloc(sizeof(queue));
	wait_queue->head = NULL;
	wait_queue->tail = NULL;

	act.sa_handler = switchTimer;
	act.sa_flags = SA_SIGINFO;
	sigaction(SIGALRM, &act, NULL);


	
}







/* give CPU possession to other user-level worker threads voluntarily */
int worker_yield() {

	
    schedulerFlag = YIELD;

	if(current->value->priority == HIGH){

		if(schedQueue->highest_priority_queue->head->next == NULL){
			return 0;
		}else{
			schedulerFlag = YIELD;
			schedule();
		}

	}else if(current->value->priority == MEDIUM){
		if(schedQueue->highest_priority_queue->head->next == NULL){
			return 0;
		}else{
			schedulerFlag = YIELD;
			schedule();
		}

	}else if(current->value->priority == MEDIUM_LOW){
		if(schedQueue->medium_low_priority_queue->head->next == NULL){
			return 0;
		}else{
			schedulerFlag = YIELD;
			schedule();
		}

	}else{

		if(schedQueue->lowest_priority_queue->head->next == NULL){
			return 0;
		}else{
			schedulerFlag = YIELD;
			schedule();
		}
	}

	return 0;

	schedule();

	
	
	// - change worker thread's state from Running to Ready
	// - save context of this thread to its thread control block
	// - switch from thread context to scheduler context

	// YOUR CODE HERE
	
	//curent_thread->status = Ready;
	//enqueue(highest_priority_queue, curent_thread);
	//swapcontext(&curent_thread->cont, &sched);
	
	return 0;
};

/* terminate a thread */
int worker_exit(void *value_ptr) {

	

	//printThreads();


	if(current == NULL){
		return 0;
	}
    
	//compute the turnaround time for the current thread
	current->value->time_finished = clock();
	turnaround_times += (float)(current->value->time_finished - current->value->time_created) / CLOCKS_PER_SEC ;

	int length = getQueueLength(join_queue);
	extied_process * exiting_pr = (extied_process *)malloc(sizeof(extied_process));

	exiting_pr->process = current->value;
	exiting_pr -> next = NULL;
	
	exiting_pr->val_ptr = value_ptr;

	schedulerFlag = EXIT;

	node_t * tmp = NULL;
	for(int i = 0; i < length; i++){
		tmp = dequeue(join_queue);

		if(tmp->value->jid != current->value->jid){
			enqueue(tmp, schedQueue->highest_priority_queue);
		}else{
			tmp->value->val_ptr = value_ptr;
			tmp->value->priority = HIGH;
			enqueue(tmp, schedQueue->highest_priority_queue);
		}

	}

	addToExitList(exiting_pr);
	//calcuateStatisitcs

	exitCount++;
	if(exitCount == thread_cnt -1 && exitCount != 1){
		rapport();
	}
	
	schedule();
	return 0;
};

void addToExitList(extied_process *exiting_pr){
	if (extied_processes == NULL) {
		extied_processes = exiting_pr;
	} else {
		exiting_pr->next = extied_processes;
		extied_processes = exiting_pr;
	}
}

/* Wait for thread termination */
int worker_join(worker_t thread, void **value_ptr) {

	extied_process * temp = extied_processes;
	
	if ((uint)thread > thread_cnt-1) {
	
		return -1;
	}
	
	while (temp != NULL) {
		if (temp->process->tid == thread) {
			if (value_ptr != NULL) {
				*value_ptr = temp->val_ptr;
			}
			return 0;
		} else {
			temp = temp -> next;
		}
	}
	
	current->value->jid = thread;

	//compute the turnaround time for the current thread
	current->value->time_finished = clock();
	turnaround_times += (float)(current->value->time_finished - current->value->time_created) / CLOCKS_PER_SEC ;
	
	schedulerFlag = JOIN;
	
	schedule();
	
	if (value_ptr != NULL) {
		*value_ptr = current->value->val_ptr;
	}
	
	
	return 0;
};

int getQueueLength(queue* queue){
	node_t * tmp = queue->head;
	int count = 0;
	while(tmp != NULL){
		count++;
		tmp = tmp->next;
	}
	return count;
}

/* initialize the mutex lock */
int worker_mutex_init(worker_mutex_t *mutex,  void** arg) {

	if (mutex == NULL) {
		return -1;
	}
	mutex_cnt++;
	mutex->mutex_id = mutex_cnt;
	mutex->state = unused;
	mutex->owner_id = -1;
	return 0;

	
};

/* aquire the mutex lock */
int worker_mutex_lock(worker_mutex_t *mutex) {
	
	if (mutex == NULL) {
		return -1;
	}

	if(mutex->state == Locked){
		return -1;
	}
	if(mutex->owner_id != current->value->tid){
		return -1;
	}

	
	node_t * tmp = wait_queue->head;

	while(mutex->state == Locked){
		schedulerFlag = BLOCKED;
		current->value->mutex_id = mutex->mutex_id;
		schedule();
	}

	

	mutex->state = Locked;
	current->value->mutex_id = mutex->mutex_id;
    return 0;

		
};



/* release the mutex lock */
int worker_mutex_unlock(worker_mutex_t *mutex) {
	if(mutex->owner_id != current->value->tid){
		return -1;
	}

	if(mutex->state == Locked){
		return -1;
	}
	if(mutex->owner_id != current->value->tid){
		return -1;
	}

	mutex->state = Ready_Waiting;
	
	node_t * tmp = wait_queue->head;

	while(tmp != NULL){
		if(mutex->mutex_id == tmp->value->mutex_id){
			completeUnlock(mutex, tmp);
			return 0;
		}
		tmp = tmp->next;
	}


	return 0;
};

void completeUnlock(worker_mutex_t *mutex, node_t *tmp){
	tmp->value->mutex_id = -1;
    tmp->value->priority = HIGH;
	mutex->state = Ready_Waiting;
	enqueue(dequeue(wait_queue), schedQueue->highest_priority_queue);
}


/* destroy the mutex */
int worker_mutex_destroy(worker_mutex_t *mutex) {

	
	if(mutex->owner_id != current->value->tid){
		return -1;
	}

	if(mutex->state == Locked){
		return -1;
	}
	if(mutex->owner_id != current->value->tid){
		return -1;
	}

	node_t * temp = wait_queue->head;
	while (temp != NULL) {
		if (mutex->mutex_id == temp->value->mutex_id) {
			return -1;
		}
		temp = temp->next;
	}
	//free(mutex);
	mutex = NULL;
	
	return 0;
	// - de-allocate dynamic memory created in worker_mutex_init
	//if(mutex == NULL) return -1;

	//mutex->state = NULL;
	//mutex->owner = 0;
	//free(mutex);
};

/* scheduler */
static void schedule() {
	// - every time a timer interrupt occurs, your worker thread library 
	// should be contexted switched from a thread context to this 
	// schedule() function

	// - invoke scheduling algorithms according to the policy (RR or MLFQ)

	// if (sched == RR)
	//		sched_rr();
	// else if (sched == MLFQ)
	// 		sched_mlfq();

	// YOUR CODE HERE
	
// - schedule policy
#ifndef MLFQ
	// Choose RR
	sched_rr();
#else 
	// Choose MLFQ
	sched_mlfq();
#endif

}

void printThreads() {

	node_t * temp = schedQueue->highest_priority_queue->head;

	
	//printf("THREAD COUNT: %d \n", thread_cnt);

	while (temp != NULL) {
			
			printf("HIGH PRIOTITY: THREAD ID: %d \n", temp->value->tid);
			temp = temp->next;
			
	}

	temp = schedQueue->medium_priority_queue->head;
	
	while (temp != NULL) {
			
			printf("MEDIUM PRIOTITY: THREAD ID: %d \n", temp->value->tid);
			temp = temp->next;
			
	}
	temp = schedQueue->medium_low_priority_queue->head;
	while (temp != NULL) {
			
			printf("MEDIUM_LOW PRIOTITY: THREAD ID: %d \n", temp->value->tid);
			temp = temp->next;
			
	}

	temp = schedQueue->lowest_priority_queue->head;

	while (temp != NULL) {
			
			printf("LOW PRIOTITY: THREAD ID: %d \n", temp->value->tid);
			temp = temp->next;
			
	}

	
}

int updateRR(){

	if(schedulerFlag == INACTIVE){

		
		return 1;
	}

	if(schedulerFlag == FIRST){
		return 0;
	}
	if(schedulerFlag == YIELD){
		
		enqueue(dequeue(schedQueue->highest_priority_queue), schedQueue->highest_priority_queue);
		schedulerFlag = INACTIVE;
	}
	if(schedulerFlag == TIMER){
		//printf("TIMER \n");
		enqueue(dequeue(schedQueue->highest_priority_queue), schedQueue->highest_priority_queue);
		schedulerFlag = INACTIVE;
	}
	if(schedulerFlag == EXIT){
		
		dequeue(schedQueue->highest_priority_queue);
		if(current->value->tid != 0){
			free(current->value->cont->uc_stack.ss_sp);
	    	free(current->value->cont);
	    	free(current->value);
	    	free(current);
		}
		current = NULL;
		schedulerFlag = INACTIVE;
	}
	if(schedulerFlag == JOIN){
		
		enqueue(dequeue(schedQueue->highest_priority_queue), join_queue);
		schedulerFlag = INACTIVE;
	}

	if(schedulerFlag == BLOCKED){
		
		enqueue(dequeue(schedQueue->highest_priority_queue), wait_queue);
		schedulerFlag = INACTIVE;
	}

	return 0;
	

}

void switchTimer(){
	
	schedulerFlag = TIMER;
	schedule();
}

/* Round-robin (RR) scheduling algorithm */


static void sched_rr() {

	

	//Checks if context switch needs to be made and the queues that have to be updated
	if(updateRR() == 0){
		
		
		node_t * new_cont = NULL;

		node_t * tmp = schedQueue->highest_priority_queue->head;
		while(tmp != NULL) {
			//printf("%d \n", prnt->thread_block->tid);
			tmp = tmp->next;
		}

		new_cont = schedQueue->highest_priority_queue->head;

		if(new_cont == NULL){
			return;
		}
		if(current != NULL){
			if(current->value->tid == new_cont->value->tid){
				
				if (new_cont->next == NULL) {
					
				}else{
					new_cont = new_cont->next;
				}

				requeue();
				if(new_cont == NULL){
				    new_cont = current;
			    }
			}

			
		}


		it_val.it_value.tv_sec =     500/100000;
        it_val.it_value.tv_usec =    (500*1000) % 1000000;	
        it_val.it_interval = it_val.it_value;
        if (setitimer(ITIMER_REAL, &it_val, NULL) == -1) {
         printf("error calling setitimer() \n");
         exit(1);
        }

		

		//node_t * old = current;

		if (schedulerFlag != EXIT) {
			node_t * old = current;
			if(current == NULL){
				
                current = new_cont;
				setcontext(new_cont->value->cont);
			}else{
				current = new_cont;
			    swapcontext(old->value->cont, new_cont->value->cont);
			}
			
		} else {
			
			schedulerFlag = INACTIVE;
			current = new_cont;

			if(current->value->first_scheduled == 0){
					current->value->first_scheduled = 1;
					current->value->first_time_scheduled = clock();
					response_times += (float)(current->value->first_time_scheduled - current->value->time_created) / CLOCKS_PER_SEC ;
			}

			setcontext(new_cont->value->cont);
			
			
		}
	}

	
}

void requeue(){
	if(current->value->priority == HIGH){
		enqueue(dequeue(schedQueue->highest_priority_queue), schedQueue->highest_priority_queue);
	}
	if(current->value->priority == MEDIUM){
		enqueue(dequeue(schedQueue->medium_priority_queue), schedQueue->medium_priority_queue);
	}
	if(current->value->priority == MEDIUM_LOW){
		enqueue(dequeue(schedQueue->medium_low_priority_queue), schedQueue->medium_low_priority_queue);
	}else{
		enqueue(dequeue(schedQueue->lowest_priority_queue), schedQueue->lowest_priority_queue);
	}
}

void demote(){

	
	if(current->value->priority == HIGH){
		current->value->priority = MEDIUM;
		enqueue(dequeue(schedQueue->highest_priority_queue), schedQueue->medium_priority_queue);
	}
	else if(current->value->priority == MEDIUM){
		current->value->priority = MEDIUM_LOW;
		enqueue(dequeue(schedQueue->medium_priority_queue), schedQueue->medium_low_priority_queue);
	}else if(current->value->priority == MEDIUM_LOW){
		current->value->priority = LOW;
		enqueue(dequeue(schedQueue->medium_low_priority_queue), schedQueue->lowest_priority_queue);
	}else{
		enqueue(dequeue(schedQueue->lowest_priority_queue), schedQueue->lowest_priority_queue);
	}
	
}

void cleanse(){

	while(!isEmpty(schedQueue->lowest_priority_queue)){
		enqueue(dequeue(schedQueue->lowest_priority_queue), schedQueue->highest_priority_queue);
	}
	while(!isEmpty(schedQueue->medium_low_priority_queue)){
		enqueue(dequeue(schedQueue->medium_low_priority_queue), schedQueue->highest_priority_queue);
	}
	while(!isEmpty(schedQueue->medium_priority_queue)){
		enqueue(dequeue(schedQueue->medium_priority_queue), schedQueue->highest_priority_queue);
	}

	node_t * quHead = schedQueue->highest_priority_queue->head;
	while(quHead != NULL){
		quHead->value->priority = HIGH;

		quHead = quHead->next;
	}
	
}

void mlfqExit(){
		if(current->value->priority == HIGH){
		    dequeue(schedQueue->highest_priority_queue);
	    }
	    if(current->value->priority == MEDIUM){
		   dequeue(schedQueue->medium_priority_queue);
	    }
	    if(current->value->priority == MEDIUM_LOW){
			dequeue(schedQueue->medium_low_priority_queue);
		  
	    }else{
		   dequeue(schedQueue->lowest_priority_queue);
	    }

		if(current->value->tid != 0){
			free(current->value->cont->uc_stack.ss_sp);
	    	free(current->value->cont);
	    	free(current->value);
	    	free(current);
		}
		
		current = NULL;
		schedulerFlag = INACTIVE;
}
void mlfqJoin(){

	if(current->value->priority == HIGH){
		enqueue(dequeue(schedQueue->highest_priority_queue), join_queue);
	}
	if(current->value->priority == MEDIUM){
		enqueue(dequeue(schedQueue->medium_priority_queue), join_queue);
	}
	if(current->value->priority == MEDIUM_LOW){
		enqueue(dequeue(schedQueue->medium_low_priority_queue), join_queue);
	}else{
		enqueue(dequeue(schedQueue->lowest_priority_queue), join_queue);
	}

}
void mlfqBlock(){
	if(current->value->priority == HIGH){
		enqueue(dequeue(schedQueue->highest_priority_queue), wait_queue);
	}
	if(current->value->priority == MEDIUM){
		enqueue(dequeue(schedQueue->medium_priority_queue), wait_queue);
	}
	if(current->value->priority == MEDIUM_LOW){
		enqueue(dequeue(schedQueue->medium_low_priority_queue), wait_queue);
	}else{
		enqueue(dequeue(schedQueue->lowest_priority_queue), wait_queue);
	}
}

int updateMLFQ(){

	if(schedulerFlag == INACTIVE){

		
		return 1;
	}

	if(schedulerFlag == FIRST){

		
		return 0;
	}
	if(schedulerFlag == YIELD){
		requeue();
		schedulerFlag = INACTIVE;
	}
	if(schedulerFlag == TIMER){
		
		demote();
		schedulerFlag = INACTIVE;
	}
	if(schedulerFlag == EXIT){
		mlfqExit();
	}
	if(schedulerFlag == JOIN){
		mlfqJoin();
		schedulerFlag = INACTIVE;
	}

	if(schedulerFlag == BLOCKED){
		
		mlfqBlock();
		schedulerFlag = INACTIVE;
	}

	if(currCycleCount > maxCycleCount){
		currCycleCount = 0;
		cleanse();

	}

	
	return 0;

};
static void sched_mlfq() {

	if(updateMLFQ() == 0){

		node_t * new_cont = NULL;

		currCycleCount++;

		int isInHigh = 0;


		node_t * topHead = schedQueue->highest_priority_queue->head;
		node_t * medHead = schedQueue->medium_priority_queue->head;
        node_t * medLowHead = schedQueue->medium_low_priority_queue->head;
		node_t * lowHead = schedQueue->lowest_priority_queue->head;

	

		int topLayerSel = 0;
		int medLayerSel = 0;
		int medLowLayerSel = 0;
		int lowLayerSel = 0;

		if(topHead != NULL){
			new_cont = topHead;
			topLayerSel = 1;
		}else if(medHead != NULL){
			new_cont = medHead;
			medLayerSel = 1;
		}else if(medLowHead != NULL){
			new_cont = medLowHead;
			medLowLayerSel = 1;
		}else if(lowHead != NULL){
			new_cont = lowHead;
			lowLayerSel = 1;
		}else{
			return;
		}


		

		if(current == NULL){

		}else{

			
			if(current->value->tid == new_cont->value->tid){
				
				if (new_cont->next == NULL) {
					if(topLayerSel){
						if(medHead != NULL){
							new_cont = medHead;
						}else if(medLowHead != NULL){
							new_cont = medLowHead;
						}else if(lowHead != NULL){
							new_cont = lowHead;
						}else{
							new_cont = NULL;
						}
					}
					if(medLayerSel){
						if(medLowHead != NULL){
							new_cont = medLowHead;
						}else if(lowHead != NULL){
							new_cont = lowHead;
						}else{
							new_cont = NULL;
						}
					}
					if(medLowLayerSel){
						if(lowHead != NULL){
							new_cont = lowHead;
						}else{
							new_cont = NULL;
						}
					}else{
						new_cont = NULL;
					}
					
				}else{
					new_cont = new_cont->next;
				}

			   requeue();			

			   if(new_cont == NULL){
				   new_cont = current;
			    }
			}

			


			

		}
		
		
		it_val.it_value.tv_sec =     500/100000;
       it_val.it_value.tv_usec =    (50*1000) % 1000000;	
        it_val.it_interval = it_val.it_value;
       if (setitimer(ITIMER_REAL, &it_val, NULL) == -1) {
         printf("error calling setitimer() \n");
         exit(1);
        }

		if (schedulerFlag != EXIT) {
			node_t * old = current;
			if(current == NULL){
				
                current = new_cont;
				setcontext(new_cont->value->cont);
			}else{
				current = new_cont;
			    swapcontext(old->value->cont, new_cont->value->cont);
			}
			
		} else {
			
			schedulerFlag = INACTIVE;
			current = new_cont;

			if(current->value->first_scheduled == 0){
					current->value->first_scheduled = 1;
					current->value->first_time_scheduled = clock();
					response_times += (float)(current->value->first_time_scheduled - current->value->time_created) / CLOCKS_PER_SEC ;
			}

			setcontext(new_cont->value->cont);
			
			
		}

	}

	//printThreads();

	
	// - your own implementation of MLFQ
	// (feel free to modify arguments and return types)

	// YOUR CODE HERE
	
}

void rapport(){

		printf("Average Turnaround time : %f\n", turnaround_times/exitCount);
		printf("Average Response Time : %f\n", response_times/exitCount);
}