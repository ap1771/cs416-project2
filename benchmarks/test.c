#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>
#include "../worker.h"


/* A scratch program template on which to call and
 * test worker library functions as you implement
 * them.
 *
 * You can modify and use this program as much as possible.
 * This will not be graded.
 */

worker_mutex_t* mutex;

int counter = 0;


void* printd(){

   for(int i = 0; i <= 10; i++){
	worker_mutex_lock(mutex);
	counter++;
	worker_mutex_unlock(mutex);
   }
}



int main(int argc, char **argv) {

	/* Implement HERE */
	worker_t w = 1;

	worker_t w2 = 2;

	worker_mutex_init(mutex, NULL);

	worker_create(&w, NULL, printd, NULL);

	worker_create(&w2, NULL, printd, NULL);

	

	worker_join(w,NULL);
	worker_join(w2, NULL);

	printf("%d\n", counter);

	worker_mutex_destroy(mutex);
	

	return 0;
}
