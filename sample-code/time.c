#include <stdio.h>
#include <time.h> //clock , clock(), CLOCK_per_second
#include <unistd.h>	

void printd(){
    for (int i = 0; i < 10; i++){
       printf("%d - ", i);
    }
    printf("\n");
}

int main () {
   clock_t start = clock();
  
   printf("Clock per second : %d\n", (int)CLOCKS_PER_SEC);
   printd();

   clock_t end = clock();
    printf("Execution duration : %f\n", (float)((end-start)/CLOCKS_PER_SEC));
   return(0);
}